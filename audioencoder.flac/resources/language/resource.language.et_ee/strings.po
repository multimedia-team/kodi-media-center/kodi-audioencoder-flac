# Kodi Media Center language file
# Addon Name: Flac Audio Encoder
# Addon id: audioencoder.flac
# Addon Provider: Team Kodi
msgid ""
msgstr ""
"Project-Id-Version: KODI Addons\n"
"Report-Msgid-Bugs-To: translations@kodi.tv\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2024-03-06 21:18+0000\n"
"Last-Translator: rimasx <riks_12@hot.ee>\n"
"Language-Team: Estonian <https://kodi.weblate.cloud/projects/kodi-add-ons-audio-decodersencoders/audioencoder-flac/et_ee/>\n"
"Language: et_ee\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.4\n"

msgctxt "Addon Summary"
msgid "FLAC (Free Lossless Audio Codec) Encoder"
msgstr "FLAC (Free Lossless Audio Codec) enkooder"

msgctxt "Addon Description"
msgid "FLAC stands for Free Lossless Audio Codec, an audio format similar to MP3, but lossless, meaning that audio is compressed in FLAC without any loss in quality. This is similar to how Zip works, except with FLAC you will get much better compression because it is designed specifically for audio, and you can play back compressed FLAC files in your favorite player (or your car or home stereo, see supported devices) just like you would an MP3 file.[CR][CR]FLAC stands out as the fastest and most widely supported lossless audio codec, and the only one that at once is non-proprietary, is unencumbered by patents, has an open-source reference implementation, has a well documented format and API, and has several other independent implementations."
msgstr "FLAC on Free Lossless Audio Codec lühendatult, sarnane MP3-ga, kuid kadudeta helivorming, mis tähendab, et heli tihendatakse FLAC-is kvaliteedis kaotamata. See sarnaneb Zip pakkimisele, aga FLAC tagab palju parema tihenduse, kuna see on loodud spetsiaalselt heli jaoks ja tihendatud FLAC faile saab sarnaselt MP3 failidega taasesitada lemmikpleieris (autos või koduses stereos, vaata toetatud seadmeid).[CR][CR]FLAC on kiireim ja kõige laialdasemalt toetatud kadudeta helikoodek ning ainus sõltumatu patendivaba ja avatud lähtekoodiga teostusega. Sellel on hästi dokumenteeritud vorming ja API ning mitu muud sõltumatut rakendust."

msgctxt "#30000"
msgid "Compression level"
msgstr "Tihendustase"

msgctxt "#30001"
msgid "FLAC uses a compression level parameter that varies from 0 (fastest) to 8 (slowest). The compressed files are always perfect, lossless representations of the original data. Although the compression process involves a tradeoff between speed and size, the decoding process is always quite fast and not dependent on the level of compression."
msgstr "FLAC kasutab tihendustaseme parameetrit, mis varieerub vahemikus 0 (kiireim) kuni 8 (aeglasem). Tihendatud failid on alati originaalandmete täiuslikud, kadudeta versioonid. Kuigi tihendusprotsess hõlmab kompromissi kiiruse ja suuruse vahel, on dekodeerimisprotsess alati üsna kiire ega sõltu tihendamise tasemest."
